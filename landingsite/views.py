from django.http import HttpResponse
from django.shortcuts import render

def hello(response):
    return HttpResponse("Hello world!")

def base(response):
    return render(response, "base.html")

def index(response):
    lipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in magna nulla. \
        Vivamus vitae gravida neque. Sed in sapien vitae tellus ultricies sagittis. \
        Nullam ultrices congue augue, at molestie sem tempus vel. \
        Donec sollicitudin augue vulputate, eleifend enim et, iaculis urna. \
        Praesent in tortor porta enim ultrices porta ut nec leo. Ut sed tellus pellentesque, \
        tincidunt lectus nec, varius justo. Pellentesque et laoreet magna, nec rhoncus diam. Nulla arcu justo, \
        accumsan id hendrerit ut, fermentum vitae lorem. Aliquam et venenatis dui."
    phones = ["iPhone", "Samsung", "Nokia"]

    context = {
        "title": "Welcome to the Cupcakes Site",
        "username": "Aeriel",
        "phones": phones,
        "lipsum": lipsum
    }
    return render(response, "index.html", context)

def about(response):
    info = "A cupcake (also British English: fairy cake; Hiberno-English: bun; \
    Australian English: fairy cake or patty cake) is a small cake designed to serve one person, \
    which may be baked in a small thin paper or aluminum cup. \
    As with larger cakes, icing and other cake decorations such as fruit and candy may be applied."

    history = "The earliest extant description of what is now often called a cupcake was in 1796, \
    when a recipe for a light cake to bake in small cups was written in American Cookery by Amelia Simmons. \
     The earliest extant documentation of the term cupcake itself was in Seventy-five Receipts for Pastry, Cakes, \
     and Sweetmeats in 1828 in Eliza Leslie's Receipts cookbook."

    context = {
        "title": "About",
        "info": info,
        "history": history
    }
    return render(response, "about.html", context)

def contact(response):
    context = {
        "title": "Contact"
    }
    return render(response, "contact.html", context)