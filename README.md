![](screenshot.png =300x)

## Setup
---
Create our virtual environment

    mkvirtualenv landingsite

Access the virtual environment we just made

    workon landingsite

Install the python requirements

    pip install django

Clone the project

    git clone https://aerielcruz@bitbucket.org/aerielcruz/landingsite.git

Get inside the project

    cd landingsite

Run the server

    ./manage.py runserver

Open `localhost:8000` in your browser